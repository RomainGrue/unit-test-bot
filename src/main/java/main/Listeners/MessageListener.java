package main.Listeners;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Objects;


public class MessageListener extends ListenerAdapter {
    public void onMessageReceived(MessageReceivedEvent event){
        Objects.requireNonNull(event);

        var channel = event.getChannel();
        var message = event.getMessage();
        if(containsAttachment(message)){
            var attach = message.getAttachments().get(0);

            sendReceivedConfirmation(channel);
            if(isAttachmentValid(attach)){
                sendAttachmentConfirmation(channel);
            }
        }
    }

    private boolean containsAttachment(Message m){
        return !m.getAttachments().isEmpty();
    }

    private boolean isAttachmentValid(Message.Attachment attachment){
        return attachment.getFileExtension().equals("java");
    }

    private void sendReceivedConfirmation(MessageChannel channel){
        channel.sendMessage("Message Received !")
                .queue();
    }

    private void sendAttachmentConfirmation(MessageChannel channel){
        channel.sendMessage(".Java Attachment Received !")
                .queue();
    }
}
